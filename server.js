const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcryptjs = require("bcryptjs");
const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");

app.use(passport.initialize());

app.use(bodyParser.json());

app.listen(3030, () => {
  console.log("connected");
});
const user = {
  id: "1",
  username: "ZakiFarokhi",
  password: "Farokhi96^",
};
const secretKey = "iniAdalahSebuahSecretKey";
var hashedPassword;
app.get("/", (req, res) => {
  res.status(200).json({
    connected: true,
  });
});
app.post("/register", (req, res) => {
  hashedPassword = bcryptjs.hashSync(user.password, 10);
  res.status(200).json({
    id: user.id,
    username: user.username,
    password: bcryptjs.hashSync(user.password, 10),
  });
});

app.post("/login", async (req, res) => {
  const { username, password } = req.body;
  if (username !== user.username)
    res.status(403).json({ messge: "username not found" });

  const checkPassword = bcryptjs.compare(user.password, hashedPassword);
  if (!checkPassword) res.status(403).json({ messge: "wrong password" });

  res.status(200).json({
    id: user.id,
    username: user.username,
    token: jwt.sign({ id: user.id, username: user.user }, secretKey, {
      expiresIn: "100000",
    }),
  });
});

const libPassport = require("./lib/passport");
app.get(
  "/whoami",
  libPassport.authenticate("jwt", { session: false }),
  (req, res) => {
    const currentUser = req.user;
    res.status(200).json({
      data: currentUser,
    });
  }
);

app.get("/whoami2", (req, res) => {
  jwt.verify(req.headers.authorization, secretKey, (err, decode) => {
    res.status(200).json({
      data: decode,
    });
  });
});

app.get("/decode", async (req, res) => {
  try {
    const decode = await jwt.decode(req.headers.authorization, {
      complete: true,
    });
    res.status(200).json({
        data:decode
    })
  } catch (error) {}
});
