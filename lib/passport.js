const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')

const user = {
    id : "1",
    username : "ZakiFarokhi",
    password : "$2a$10$6hGEWMq8kG.uYU/EA8kkFOkmlwg.ocBJVFqS4UD/Q32Fdow8O2wYa"
}

/* Passport JWT Options */
const options = {
 // Untuk mengekstrak JWT dari request, dan mengambil token-nya dari header yang bernama Authorization
 jwtFromRequest: ExtractJwt.fromHeader('authorization'),

 /* Harus sama seperti dengan apa yang kita masukkan sebagai parameter kedua dari jwt.sign di User Model.
    Inilah yang kita pakai untuk memverifikasi apakah tokennya dibuat oleh sistem kita */
 secretOrKey: 'iniAdalahSebuahSecretKey',
 }

passport.use(new JwtStrategy(options, async (payload, done) => {
 // payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign
 const userFound = (payload.id === user.id)? done(null, user) : done(err, false)
//  User.findByPk(payload.id)
//    .then(user => done(null, user))
//    .catch(err => done(err, false))
}))

module.exports = passport
